package Kamera;

import java.util.ArrayList;
import java.util.List;

abstract public class Vehicle{
    private String regNumber;
    private List<Person> ownersList = new ArrayList<>();
    private boolean isMotor;

    //constructor
    public Vehicle(){

    };
    public Vehicle(String RegNumber,boolean IsMotor){
        this.isMotor = IsMotor;
        this.regNumber = RegNumber;
    }

    public String getRegNumber() {
        return regNumber;
    }
    public void setRegNumber(String regNumber) {
        this.regNumber = regNumber;
    }

    public boolean isMotor() {
        return isMotor;
    }
    public void setMotor(boolean motor) {
        isMotor = motor;
    }

    public void addToOwnersList(Person person){
        this.ownersList.add(person);
    }

    public String getOwnersList(){
       return ownersList  + "\n";
    }

    public List getListOfOwners(){
       return ownersList;
    }

    @Override
    public String toString() {
        return "regNumber: " + regNumber + ", Motor: " + isMotor + ", Type: " + this.getClass() +"\n";
    }

}
