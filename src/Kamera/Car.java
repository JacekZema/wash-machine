package Kamera;

public class Car extends MotorVehicle {
    private int maxPassegers;
    private int passagers;

    //public Car(){};
    public Car(String RegNumber, double engineCapacity, String fuelType, int maxPassegers, int passagers) {
        super(RegNumber, engineCapacity, fuelType);
        this.maxPassegers = maxPassegers;
        this.passagers = passagers;
    }

    public int getMaxPassegers() {
        return maxPassegers;
    }

    public void setMaxPassegers(int maxPassegers) {
        this.maxPassegers = maxPassegers;
    }

    public int getPassagers() {
        return passagers;
    }

    public void setPassagers(int passagers) {
        this.passagers = passagers;
    }

    @Override
    public boolean isOverload(){
        if (maxPassegers > passagers)
        {
            return false;
        }
        else{
            return true;
        }
    }
}
