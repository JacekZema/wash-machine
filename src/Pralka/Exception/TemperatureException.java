package Pralka.Exception;

public class TemperatureException extends Exception {
    public TemperatureException() {
        super("This temp is out of range");
    }
}
