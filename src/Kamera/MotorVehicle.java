package Kamera;

abstract public class MotorVehicle extends Vehicle {
    private double engineCapacity;
    private String fuelType;

    public MotorVehicle(){};
    public MotorVehicle(String RegNumber, double engineCapacity, String fuelType) {
        super(RegNumber, true);
        this.engineCapacity = engineCapacity;
        this.fuelType = fuelType;
    }

    public abstract boolean isOverload();

    public double getEngineCapacity() {
        return engineCapacity;
    }
    public void setEngineCapacity(double engineCapacity) {
        this.engineCapacity = engineCapacity;
    }

    public String getFuelType() {
        return fuelType;
    }
    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }
}
