package Kamera;

public class Truck extends MotorVehicle{
    private double maxCapacity;
    private double weight;

    public Truck (){};
    public Truck(String RegNumber, double engineCapacity, String fuelType, double maxCapacity, double weight) {
        super(RegNumber, engineCapacity, fuelType);
        this.maxCapacity = maxCapacity;
        this.weight = weight;
    }

    @Override
    public boolean isOverload() {
        if (maxCapacity > weight)
        {
            return false;
        }
        else{
            return true;
        }
    }

    public double getMaxCapacity() {
        return maxCapacity;
    }
    public void setMaxCapacity(double maxCapacity) {
        this.maxCapacity = maxCapacity;
    }

    public double getWeight() {
        return weight;
    }
    public void setWeight(double weight) {
        this.weight = weight;
    }
}
