package Pralka;

import Pralka.Exception.ProgramException;
import Pralka.Exception.SpinSpeedException;
import Pralka.Exception.TemperatureException;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Controller {
    public static void setProgramMain(WashMachine washMachine, int program){
        try{
            washMachine.setProgram(program);
            System.out.println("Current program is: " + washMachine.getProgram());
        }
        catch (ProgramException e){
            e.printStackTrace();
        }
    }
    public static void setTemperatureMain(WashMachine washMachine, float temperature){
        try{
            washMachine.setTemperature(temperature);
            System.out.println("Current temperature is: " + washMachine.getTemperature() + "\u00b0C");
        }
        catch (TemperatureException e)
        {
            e.printStackTrace();
        }
    }

    public static void tempDownMain(WashMachine washMachine){
        try{
            washMachine.tempDown();
            System.out.println("Current temperature is: " + washMachine.getTemperature() + "\u00b0C");

        }
        catch (TemperatureException e){
            e.printStackTrace();
        }

    }
    public static void tempUpMain(WashMachine washMachine){
        try{
            washMachine.tempUp();
            System.out.println("Current temperature is: " + washMachine.getTemperature() + "\u00b0C");

        }
        catch (TemperatureException e){
            e.printStackTrace();
        }

    }

    public static void setSpinSpeedMain(WashMachine washMachine, int speed){
        try{
            washMachine.setSpinSpeed(speed);
            System.out.println("Current speed: " + washMachine.getSpinSpeed());
        }
        catch (SpinSpeedException e){
            e.printStackTrace();
        }
    }
    public static void spinSpeedUpMain(WashMachine washMachine){
        washMachine.spinSpeedUp();
        System.out.println("Current speed: " + washMachine.getSpinSpeed());
    }
    public static void spinSpeedDownMain(WashMachine washMachine){
        washMachine.spinSpeedDown();
        System.out.println("Current speed: " + washMachine.getSpinSpeed());
    }

    public static void showStatusMain(WashMachine washMachine){
        System.out.println("Name : "
                + washMachine.name
                + " Current temperature: "
                + washMachine.getTemperature()
                + "\u00b0C "
                + " Current program: "
                + washMachine.getProgram()
                + " Current speed: "
                + washMachine.getSpinSpeed());
    }

    public static void nextProgramMain(WashMachine washMachine){
        washMachine.nextProgram();
        System.out.println("Current program: " + washMachine.getProgram());
    }
    public static void previousProgramMain(WashMachine washMachine){
        washMachine.previousProgram();
        System.out.println("Current program: " + washMachine.getProgram());
    }

    public static void sortingList(List<WashMachine> list){
        System.out.println("List before sorting: ");
        list.stream().forEach( obj -> showStatusMain(obj) );
        Collections.sort(list, COMPARE_BY_NAME);
        System.out.println("List after sorting: ");
        list.stream().forEach( obj -> showStatusMain(obj) );
    }

    public static Comparator<WashMachine> COMPARE_BY_NAME = new Comparator<WashMachine>() {
        @Override
        public int compare(WashMachine o1, WashMachine o2) {
            return o1.getName().name().compareTo(o2.getName().name());
        }
    };
}
