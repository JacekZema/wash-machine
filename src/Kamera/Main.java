package Kamera;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static List<Person> personList = new ArrayList<>();
    public static List<Vehicle> vehicleList = new ArrayList<>();
    public static void showPersonList(){
        for(Person i: personList) {
            System.out.println(i.getName() + " " + i.getLastName() + " " + i.getYearsOld());
            System.out.println(i.getVehiclesList());
        }

    }
    public static void showVehicleList(){
        for(Vehicle i: vehicleList) {
            System.out.println(i.getClass() + ", Number: " + i.getRegNumber() + ", Motor inside: " + i.isMotor());
            System.out.println("Owners: ");
            System.out.println(i.getOwnersList());

        }

    }

    public static void personCreatorAndVehicleAdder(String name, String lastName, int yearsOld, Vehicle... args ){
        Person person = new Person(name,lastName,yearsOld);
        for(Vehicle arg: args)
        {
            person.addVehicle(arg);
            arg.addToOwnersList(person);
            vehicleList.add(arg);
        }
        personList.add(person);

    }
    public static void vehicleCreatorWithPerson(Vehicle vehicle, Person... args){
     vehicleList.add(vehicle);
     for(Person arg: args)
     {
         vehicle.addToOwnersList(arg);
         personList.add(arg);
     }
    }


    //2
    public static double averageCapacity(){
        double allEngineCapacity = 0;
        int allMotorVehicle = 0;
        for(Vehicle obj: vehicleList )
        {
            if(obj.isMotor() == true)
            {
                allEngineCapacity += ((MotorVehicle)obj).getEngineCapacity();
                allMotorVehicle++;
            }
        }
        return allEngineCapacity/allMotorVehicle;
    }

    //3
    public static String ifExistByNumber(){
        System.out.println("Type number od Vehicle");
        Scanner in = new Scanner(System.in);
        String s = in.nextLine();
        for(Vehicle obj: vehicleList)
        {
            if(obj.getRegNumber().equals(s) && obj.isMotor() == true)
            {
                return (((MotorVehicle)obj).getEngineCapacity() + " " + ((MotorVehicle)obj).getFuelType() + " " + obj.getOwnersList() );
            }
            else if (obj.getRegNumber().equals(s) && obj.isMotor() == false)
            {
                return ("Bicycle, type: " + (((Bicycle)obj).getTypeOfBike()) +  " " + obj.getOwnersList() );
            }

        }
        return s + " not exist";
    }

    //4
    public static void getNumberByMaxPersons(){
        int maxPassagersInVehicleList =0;
        for(Vehicle arg: vehicleList){
            //System.out.println(maxPassagersInVehicleList);
           // System.out.println(arg.getClass().getName());
            if(arg.getClass().getName() == "Kamera.Car")
            {
                if(((Car)arg).getMaxPassegers() > maxPassagersInVehicleList)
                    maxPassagersInVehicleList = ((Car)arg).getMaxPassegers();
            }
        }

        for(Vehicle arg: vehicleList){
            if(arg.getClass().getName() == "Kamera.Car")
            {
                if(((Car)arg).getMaxPassegers() == maxPassagersInVehicleList)
                    System.out.println(arg.getRegNumber());
            }
        }
    }

    //5
    public static void isCarAndBicycleInPerson(){
        for(Person arg: personList)
        {
            if(arg.getVehiclesList().contains("Kamera.Car") && arg.getVehiclesList().contains("Kamera.Bicycle") ){
                System.out.println(arg.getName() + " " + arg.getLastName());
            }

        }
    }

    public static void main(String[] args)
    {
        //1. Program przyjmuje zbiór pojazdów i zwraca wszystkich właścicieli (bez powtórzeń).
//         vehicleCreatorWithPerson(new Car("FZG13", 1000,"ON",5,4),new Person("Gruby","Grubas",25),new Person("Piękny","Kwiatek",33));
//         vehicleCreatorWithPerson(new Car("FZG14", 200,"ON",3,4),new Person("Gruby","Grubas",25),new Person("Piękny","Kwiatek",33));
//         vehicleCreatorWithPerson(new Car("FZG15", 200,"ON",4,4),new Person("Gruby","Grubas",25),new Person("Piękny","Kwiatek",33));
//         vehicleCreatorWithPerson(new Car("FZG16", 500,"ON",5,4),new Person("Gruby","Grubas",25),new Person("Piękny","Kwiatek",33));
//         vehicleCreatorWithPerson(new Bicycle("FZG17","Eldo"),new Person("Gruby","Grubas",25),new Person("Piękny","Kwiatek",33));
//
        Person franek = new Person("Franek","diabolo",23);
        Person placek = new Person("Placko","jacko",69);
        Person marek = new Person("marko","franko",19);
        Person kolega = new Person("DIABLO","ilala",98);
        Person kolega123 = new Person("MEFISTO","AKT 3",98);

        personList.add(franek); personList.add(placek); personList.add(marek); personList.add(kolega); personList.add(kolega123);

        Vehicle rower = new Bicycle("001","kolowy");
        Vehicle samochod = new Car("002",1000,"ON",5,4);
        Vehicle ciezarowka = new Truck("003",5300,"ON",2500,2450);
        Vehicle samochod1 = new Car("004",3232,"LPG",3,4);
        Vehicle samochod2 = new Car("005",234530,"FUEL",10,4);
        Vehicle samochod3 = new Bicycle("006","TURYSTYK");

        vehicleList.add(rower); vehicleList.add(samochod); vehicleList.add(ciezarowka); vehicleList.add(samochod1); vehicleList.add(samochod2); vehicleList.add(samochod3);

        franek.addVehicle(rower);
        rower.addToOwnersList(franek);
        franek.addVehicle(samochod);
        samochod.addToOwnersList(franek);
        //
        placek.addVehicle(ciezarowka);
        ciezarowka.addToOwnersList(placek);
        //
        marek.addVehicle(samochod);
        samochod.addToOwnersList(marek);
        marek.addVehicle(samochod1);
        samochod.addToOwnersList(marek);
        //
        kolega.addVehicle(rower);
        rower.addToOwnersList(kolega);
        kolega.addVehicle(samochod);
        samochod.addToOwnersList(kolega);
        kolega.addVehicle(ciezarowka);
        ciezarowka.addToOwnersList(kolega);
        kolega.addVehicle(samochod1);
        samochod1.addToOwnersList(kolega);
        //
        kolega123.addVehicle(samochod2);
        kolega123.addVehicle(samochod3);
        samochod2.addToOwnersList(kolega123);
        samochod3.addToOwnersList(kolega123);



        // 1.

        //showPersonList();
        //showVehicleList();


//        2.	Zwraca średnią pojemność silnika z całego zbioru pojazdów,
        //System.out.println("2. Average capacity of engines: " + averageCapacity());

//        3.	Przyjmuje numer rejestracyjny pojazdu i drukuje pojemność, rodzaj paliwa i właściciela
        //System.out.println(ifExistByNumber());

//        4.	Zwraca numer/numery rejestracyjny pojazdu/pojazdów z największą max liczbą pasażerów,

       // getNumberByMaxPersons();

//        5.	Zwraca właścicieli pojazdów, którzy posiadają jednocześnie samochód osobowy i motocykl,

        //isCarAndBicycleInPerson();

//        6.	Zwraca właścicieli, którzy współdzielą jakikolwiek pojazd.

        System.out.println("Persons who have more than 1 vehicle:  ");
        String tempList = "";
        for(Vehicle arg: vehicleList)
        {

            if(arg.getListOfOwners().size() > 1) {
                for(int i=0; i < arg.getListOfOwners().size();i++){
                    //System.out.println(arg.getListOfOwners().get(i).toString());
                    if(!tempList.contains(arg.getListOfOwners().get(i).toString()))
                    {
                        tempList = tempList + arg.getListOfOwners().get(i).toString() + "\n";
                    }

                }

            }
        }
        System.out.println(tempList);

    }

}

