package Pralka;

import java.util.ArrayList;
import java.util.List;

import static Pralka.Controller.*;

public class Main {

    public static void main(String[] args)
    {
        List<WashMachine> listOfWashMachine = new ArrayList<>();

        WashMachine beko = new Beko();
        WashMachine whirlpool = new Whirlpool();
        WashMachine amica = new Amica();

        listOfWashMachine.add(beko);
        listOfWashMachine.add(whirlpool);
        listOfWashMachine.add(amica);

        setProgramMain(whirlpool,25);
        setTemperatureMain(whirlpool,0);
        tempUpMain(whirlpool);
        tempDownMain(whirlpool);

        setSpinSpeedMain(whirlpool, 1000);
        spinSpeedUpMain(whirlpool);
        spinSpeedDownMain(whirlpool);

        //następny i poprzedni program (przekręcanie)
        previousProgramMain(whirlpool);
        nextProgramMain(whirlpool);

        //pokazuje status danej pralki
        showStatusMain(whirlpool);


        //zmiana temperatury
        setTemperatureMain(beko,30.63f);
        setTemperatureMain(beko,45.62f);

        //zmiana programu
        setProgramMain(whirlpool,25);
        nextProgramMain(whirlpool);
        previousProgramMain(whirlpool);
        setTemperatureMain(whirlpool,45.67f);

        //sortowanie
        sortingList(listOfWashMachine);

        //sortowanie - 2 metoda
        //listOfWashMachine.stream().sorted((o1,o2) -> o1.getName().name().compareTo(o2.getName().name())).forEach(x -> showStatusMain(x));

    }
}
