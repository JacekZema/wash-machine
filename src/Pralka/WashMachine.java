package Pralka;

import Pralka.Exception.ProgramException;
import Pralka.Exception.SpinSpeedException;
import Pralka.Exception.TemperatureException;


abstract public class WashMachine {
    private int program;
    private int spinSpeed;
    private final int maxProgram;
    private final float howTempChange;
    protected float temperature;
    protected final NamesOfWashMachine name;

    public WashMachine(float howTempChange, int maxProgram, NamesOfWashMachine name) {
        this.program = 0;
        this.temperature = 0;
        this.spinSpeed = 0;
        this.howTempChange = howTempChange;
        this.maxProgram = maxProgram;
        this.name = name;
    }

    public int getProgram() {
        return program;
    }
    public void setProgram(int program) throws ProgramException {
        if(program > 0 && program <= maxProgram){
            this.program = program;
        }
        else{
            throw new ProgramException();
        }
    }

    public void nextProgram(){
        if(program >= this.maxProgram) {
            program =1;
        }
        else
            program++;
    };
    public void previousProgram(){
        if(program <= 1 ) {
            program = this.maxProgram;
        }
        else
            program--;
    };

    public float getTemperature() {
        return this.temperature;
    }
    public void setTemperature(float temperature) throws TemperatureException {
        if ((temperature >= 0) && (temperature < 90.25)  ) {
            this.temperature = (float) (Math.round(temperature * 2) / 2.0);
        }
        else{
            throw new TemperatureException();
        }

    }

    public void tempUp() throws TemperatureException{
        if ((temperature >= 0) && (temperature < 90)  ) {
            this.temperature = temperature + howTempChange;
        }
        else{
            throw new TemperatureException();
        }

    }
    public void tempDown() throws TemperatureException{
        if ((temperature > 0) && (temperature <= 90)  ) {
            this.temperature = temperature - howTempChange;
        }
        else{
            throw new TemperatureException();
        }
    }

    public int getSpinSpeed() {
        return spinSpeed;
    }
    public void setSpinSpeed(int spinSpeed) throws SpinSpeedException {
        if(spinSpeed >= 0 && spinSpeed <= 1000)
        {
            this.spinSpeed = (Math.round( (float) spinSpeed / 100) * 100);
        }
        else{
            throw new SpinSpeedException();
        }

    }

    public void spinSpeedUp(){
        if(spinSpeed >= 0 && spinSpeed < 1000)
        {
            this.spinSpeed = spinSpeed + 100 ;
        }
        else{
            this.spinSpeed = 0;
        }
    };
    public void spinSpeedDown(){
        if(spinSpeed > 0 && spinSpeed <= 1000)
        {
            this.spinSpeed = spinSpeed - 100 ;
        }
        else{
            this.spinSpeed = 1000;
        }
    }

    public NamesOfWashMachine getName() {
        return name;
    }


}
