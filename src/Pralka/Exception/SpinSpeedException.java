package Pralka.Exception;

public class SpinSpeedException extends Exception {
    public SpinSpeedException(){
        super("This speed is out of range");
    }
}
