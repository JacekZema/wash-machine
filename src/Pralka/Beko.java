package Pralka;

import Pralka.Exception.TemperatureException;

public class Beko extends WashMachine {

    public Beko(){
        super(1,20,NamesOfWashMachine.BEKO);
    }

    @Override
    public void setTemperature(float temperature) throws TemperatureException {
        if ((temperature >= 0) && (temperature <= 90)  ) {
            this.temperature = (float) Math.round(temperature);
        }
        else{
            throw new TemperatureException();
        }
    }
}
