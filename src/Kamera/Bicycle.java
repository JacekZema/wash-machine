package Kamera;

public class Bicycle extends Vehicle{
    private String typeOfBike;

    public Bicycle(){}

    public Bicycle(String regNumber, String typeOfBike){
        super(regNumber,false);
        this.typeOfBike = typeOfBike;
    }

    public String getTypeOfBike() {
        return typeOfBike;
    }

    public void setTypeOfBike(String typeOfBike) {
        this.typeOfBike = typeOfBike;
    }
}
