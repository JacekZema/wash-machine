package Kamera;

import java.util.LinkedList;
import java.util.List;

class Person {
    private String name;
    private String lastName;
    private int yearsOld;
    private List<Vehicle> vehiclesList = new LinkedList<>();

    public Person(){}

    public Person(String name, String lastName, int yearsOld){
        this.name = name;
        this.lastName = lastName;
        this.yearsOld = yearsOld;
    }

    public String getName(){
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getYearsOld() {
        return yearsOld;
    }

    public void setYearsOld(int yearsOld) {
        this.yearsOld = yearsOld;
    }
    public void addVehicle(Vehicle vehicle){
        this.vehiclesList.add(vehicle);
    }

    public String getVehiclesList(){
       return vehiclesList +"\n";
    }

    public List getListOfVehicle(){
        return vehiclesList;
    }

    @Override
    public String toString() {
        return "Name: " + name + ", Last name: " + lastName + ", Years Old: " + yearsOld;
    }

}
