package Pralka.Exception;

public class ProgramException extends Exception {
    public ProgramException(){
        super("This program is out of range");
    }
}
